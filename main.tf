terraform {
  required_version = ">= 1.0.11"
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.2.0"
    }
  }
}
#module "sql-db" {
#  source  = "GoogleCloudPlatform/sql-db/google//modules/postgresql"
#  version = "8.0.0"
#}